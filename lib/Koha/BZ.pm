package Koha::BZ;

use Moose;
use Modern::Perl;
use JSON;
use YAML;
use REST::Client;


has client => (is => 'rw', isa => 'REST::Client');
has url => (is => 'rw', isa => 'Str');
has login => (is => 'rw', isa => 'Str');
has password => (is => 'rw', isa => 'Str');
has token => (is => 'rw', isa => 'Str');



sub get {
    my ($self, $cmd) = @_;
    my $url = $self->url . "/rest/$cmd";
    $url .= '&token=' . $self->token if $self->token;
    $self->client->GET($url);
    decode_json($self->client->responseContent());
}


sub put {
    my ($self, $cmd, $data) = @_;
    my $url = $self->url . "/rest/$cmd";
    $url .= ($url =~ /\?/ ? '&' : '?') . 'token=' . $self->token;
    $data = encode_json($data);
    #say "putting to $url";
    #say "putting with $data";
    $self->client->PUT(
        $url,
        $data,
        {
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        }
    );
    my $response = decode_json $self->client->responseContent();
    if ( exists($response->{error}) ) { 
        say Dump($response) if exists($response->{error});
        exit;
    }
}


sub post_comment {
    my ($self, $bug, $data) = @_;
    my $url = $self->url . "/rest/bug/$bug/comment";
    $url .= ($url =~ /\?/ ? '&' : '?') . 'token=' . $self->token;
    $data = encode_json($data);
    #say "putting to $url";
    #say "putting with $data";
    $self->client->POST(
        $url,
        $data,
        {
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        }
    );
    my $response = decode_json $self->client->responseContent();
    if ( exists($response->{error}) ) { 
        say Dump($response) if exists($response->{error});
        exit;
    }
}


sub BUILD {
    my $self = shift;

    $self->client( REST::Client->new() );

    if ( $self->login ) {
        my $response = $self->get('login?login=' . $self->login . "&password=" . $self->password);
        if ( $response->{error} ) {
            say "Wrong login/password to BZ";
            exit 1;
        }
        $self->token($response->{token});
    }
}

1;
